
// Objeto para pegar os preços e as fotos das camisetas

var camisetas = {
    'branca': {
        
        'gola_v': {
            'sem_estampa': {
                'preco_unit': 5.12,
                'foto': 'v-white.jpg' 
            },
            'com_estampa': {
                'preco_unit': 8.95,
                'foto': 'v-white-personalized.jpg' 
            }
        },
        
        'gola_normal': {
            'sem_estampa': {
                'preco_unit': 4.99,
                'foto': 'normal-white.jpg' 
            },
            'com_estampa': {
                'preco_unit': 8.77,
                'foto': 'normal-white-personalized.jpg' 
            }
        }
    },
    
    'colorida': {
        'gola_v': {
            'sem_estampa': {
                'preco_unit': 6.04,
                'foto': 'v-color.jpg' 
            },
            'com_estampa': {
                'preco_unit': 9.47,
                'foto': 'v-color-personalized.png' 
            }
        },
        
        'gola_normal': {
            'sem_estampa': {
                'preco_unit': 5.35,
                'foto': 'normal-color.jpg' 
            },
            'com_estampa': {
                'preco_unit': 9.28,
                'foto': 'normal-color-personalized.jpg' 
            }
        }
    }
}


// parâmetros da pesquisa

var parametros_pesquisa = {
    "quantidade": 10,
    "cor": "colorida",
    "gola": "gola_v",
    "qualidade": "q150",
    "estampa": "com_estampa",
    "embalagem": "bulk"
}


// Regras adicionais para o orçamento:

// 1. Verificar se há em localStorage os parâmetros do último orçamento e se houver, carregar a página com eles.

// 2. A camisa de qualidade alta (190g/m2) deve acrescer o preço unitário em 12%.

// 3. A embalagem unitária tem um custo de 0.15 por unidade

// 4. Após cálculo do preço, há que se aplicar um desconto por quantidade, sendo: 
    // faixa 1: acima de 1.000 - Desconto de 15%
    // faixa 2: acima de 500 - Desconto de 10%
    // faixa 3: acima de 100 - Desconto de 5%



// Resolução do desafio:

$(document).ready(function(){

    function atualizar_orcamento(parametros) {

        $('.refresh-loader').show();

        var quantidade = parametros.quantidade;
        var preco_unit = camisetas[parametros.cor][parametros.gola][parametros.estampa].preco_unit;
        var foto = "img/" + camisetas[parametros.cor][parametros.gola][parametros.estampa].foto;

        var valor_total = quantidade * preco_unit;

        if (parametros.qualidade == "q190") {
            valor_total *= 1.12;
        }

        if (parametros.embalagem == "unitaria") {
            valor_total += (quantidade * 0.15);
        }

        if (quantidade >= 1000) {
            valor_total *= 0.85;
        } else if (quantidade >= 500) {
            valor_total *= 0.90;
        }  else if (quantidade >= 100) {
            valor_total *= 0.95;
        }

        window.setTimeout(function(){

            var id_gola = "#" + parametros.gola;
            $('#result_gola').html( $(id_gola).html() );

            var id_estampa = "option[value='" + parametros.estampa +  "']";
            $('#result_estampa').html( $(id_estampa).html() );

            var id_qualidade = "#" + parametros.qualidade;
            $('#result_qualidade').html( $(id_qualidade).html() );

            var id_cor = "#" + parametros.cor;
            $('#result_cor').html( $(id_cor).html() );

            var id_embalagem = "option[value='" + parametros.embalagem +  "']";
            $('#result_embalagem').html( $(id_embalagem).html() );

            $('#result_quantidade').html( parametros.quantidade );

            $('#valor-total').html( valor_total.toLocaleString('pt-BR', { minimumFractionDigits: 2, maximumFractionDigits: 2 } ) );
            $('#foto-produto').attr("src",foto);

            $('.refresh-loader').hide();

        },1000)
    }

    function atualizar_campos (parametros) {

        // cor
        $("#cor").children().removeClass("selected");
        var id_cor = "#" + parametros.cor;
        $(id_cor).addClass("selected");

        // gola
        $("#gola").children().removeClass("selected");
        var id_gola = "#" + parametros.gola;
        $(id_gola).addClass("selected");

        // qualidade
        $("#qualidade").children().removeClass("selected");
        var id_qualidade = "#" + parametros.qualidade;
        $(id_qualidade).addClass("selected");

        // estampa
        $("#estampa").val(parametros.estampa);

        // estampa
        $("#embalagem").val(parametros.embalagem);

        // quantidade
        $("#quantidade").val(parametros.quantidade);

    }

    function atualizar_localStorage(parametros) {
        window.localStorage.setItem("quantidade", parametros.quantidade);
        window.localStorage.setItem("cor", parametros.cor);
        window.localStorage.setItem("gola", parametros.gola);
        window.localStorage.setItem("qualidade", parametros.qualidade);
        window.localStorage.setItem("estampa", parametros.estampa);
        window.localStorage.setItem("embalagem", parametros.embalagem);
    }

    if(window.localStorage["quantidade"]) {
        parametros_pesquisa.quantidade = parseInt(window.localStorage["quantidade"]);
    }

    if(window.localStorage["cor"]) {
        parametros_pesquisa.cor = window.localStorage["cor"];
    }

    if(window.localStorage["gola"]) {
        parametros_pesquisa.gola = window.localStorage["gola"];
    }

    if(window.localStorage["qualidade"]) {
        parametros_pesquisa.qualidade = window.localStorage["qualidade"];
    }

    if(window.localStorage["estampa"]) {
        parametros_pesquisa.estampa = window.localStorage["estampa"];
    }

    if(window.localStorage["embalagem"]) {
        parametros_pesquisa.embalagem = window.localStorage["embalagem"];
    }

    
    $(".option-filter div").click(function(){

        $(this).parent().children("div").removeClass("selected");
        $(this).addClass("selected");

        var categoria = $(this).parent().attr('id');
        parametros_pesquisa[categoria] = $(this).attr('id');
        atualizar_localStorage(parametros_pesquisa)
        atualizar_orcamento(parametros_pesquisa);

    });

    $("select").change(function(){

        var parametro_select = $(this).attr('id');
        parametros_pesquisa[parametro_select] = $(this).val();
        atualizar_localStorage(parametros_pesquisa)
        atualizar_orcamento(parametros_pesquisa);

    });

    $('#quantidade').change(function(){

        var parametro_input = $(this).attr('id');
        parametros_pesquisa[parametro_input] = $(this).val();
        atualizar_localStorage(parametros_pesquisa)
        atualizar_orcamento(parametros_pesquisa);

    });
    
    atualizar_campos(parametros_pesquisa);
    atualizar_orcamento(parametros_pesquisa);
});

// Sugestão de etapas da resolução

    // 1. Crie uma função para calcular o preço baseado nos parâmetros da variável "parametros_pesquisa" e solte o 
    // valor no console para testar se está certo.

    // 2. Faça os eventos click e change para os filtros.
    
        // a. Faça o evento click para os filtros do tipo botão (.option-filter). Sempre que houver um click, 
        // remova a classe "selected" dos botões do grupo e depois aplique-a apenas ao que foi clicado para
        // que ele fique azul.

        // b. Faça o evento change para os filtros do tipo <select> e para o <input> de quantidade.

        // c. Sempre que um dos eventos acima ocorrer, atualize a variável "parametros_pesquisa" e rode a função para 
        // calcular o preço

    
    // 3. Altere a função do cálculo do preço. Em vez de soltar os valores no console, atualize as informações
    // nos elementos "result_", atualize o preço no elemento "valor-total" e mude o atributo "src" do elemento 
    // "foto-produto" para alterar a imagem mostrada (todas as imagens estão na pasta img).

    // 4. Adicione a funcionalidade de hide e show do spinner (elemento "refresh-loader") à função de cálculo de preço. 
    // Como não estamos consultando dados externos, o cálculo acaba sendo rápido demais, portanto use um setTimeout 
    // para deixar ele aparecer por pelo menos 2 segundos.

    // 5. Crie a funcionalidade do localStorage e ao carregar a página, consulte o localStorage, 
    // atualize a variável "parametros_pesquisa" e rode a função de cálculo de preço

    /*  //Função para setar valores na local storage
    function atualizarLocalStorage(){
        localStorage.setItem('quantidade', parametros_pesquisa.quantidade);
        localStorage.setItem('cor', parametros_pesquisa.cor);
        localStorage.setItem('gola', parametros_pesquisa.gola);
        localStorage.setItem('qualidade', parametros_pesquisa.qualidade);
        localStorage.setItem('estampa', parametros_pesquisa.estampa);
        localStorage.setItem('embalagem', parametros_pesquisa.embalagem);
    }

    //Função pra atualizar a foto
    function atualizarFoto(){
        var path = "img/" + camisetas[parametros_pesquisa.cor][parametros_pesquisa.gola][parametros_pesquisa.estampa].foto;
        $("#foto-produto").attr("src", path);
    }

    //Função para calcular o valor
    function calcularValor(){
        $(".refresh-loader").show();

        var valorCamiseta = camisetas[parametros_pesquisa.cor][parametros_pesquisa.gola][parametros_pesquisa.estampa].preco_unit;
        var acrescimoEmbalagem = 0;
        var total = 0;

        if(parametros_pesquisa.qualidade == 'q190'){
            valorCamiseta *= 1.12;
        }
        if(parametros_pesquisa.embalagem == 'unitaria'){
            acrescimoEmbalagem = parametros_pesquisa.quantidade * 0.15;
        }

        if(parametros_pesquisa.quantidade >= 100){
            total = ((valorCamiseta * parametros_pesquisa.quantidade) + acrescimoEmbalagem) * 0.95;
        }
        else if(parametros_pesquisa.quantidade >= 500){
            total = ((valorCamiseta * parametros_pesquisa.quantidade) + acrescimoEmbalagem) * 0.90;
        }
        else if(parametros_pesquisa.quantidade >= 1000){
            total = ((valorCamiseta * parametros_pesquisa.quantidade) + acrescimoEmbalagem) * 0.85;
        }
        else{
            total = (valorCamiseta * parametros_pesquisa.quantidade) + acrescimoEmbalagem
        }        

        window.setTimeout(function(){        
            $(".refresh-loader").hide();
            $("#valor-total").html(total.toFixed(2));
        }, 1000);                    
    }

    //Setando valores padrão nos spans e local storage
    if(localStorage.getItem('cor') != null){
        if(localStorage.getItem('estampa') == 'com_estampa'){
            $("#com_estampa").addClass("selected");
            $("#sem_estampa").removeClass("selected");
        }
        else if(localStorage.getItem('estampa') != 'com_estampa'){
            $("#sem_estampa").addClass("selected");
            $("#com_estampa").removeClass("selected");
        }

        if(localStorage.getItem('embalagem') == 'bulk'){
            $("#bulk").attr("selected");
            $("#unitaria").attr("selected", "");
        }
        else if(localStorage.getItem('embalagem') != 'bulk'){
            $("#unitaria").attr("selected");
            $("#bulk").attr("selected", "");
        }

        $("#result_quantidade").html(localStorage.getItem('quantidade'));
        $("#result_cor").html($("#" + localStorage.getItem('cor')).text());
        $("#result_gola").html($("#" + localStorage.getItem('gola')).text());
        $("#result_qualidade").html($("#" + localStorage.getItem('qualidade')).text());
        $("#result_estampa").html($("#estampa").find(":selected").text());
        $("#result_embalagem").html($("#embalagem").find(":selected").text());
        atualizarFoto();
        calcularValor();
        console.log('storage');
    }
    else{
        $("#result_quantidade").html(parametros_pesquisa.quantidade);
        $("#result_cor").html($("#"+parametros_pesquisa.cor).text());
        $("#result_gola").html($("#"+parametros_pesquisa.gola).text());
        $("#result_qualidade").html($("#"+parametros_pesquisa.qualidade).text());
        $("#result_estampa").html($("#estampa").find(":selected").text());
        $("#result_embalagem").html($("#embalagem").find(":selected").text());
        atualizarFoto();
        calcularValor();
        atualizarLocalStorage();
        console.log('sem storage');
    }

    //Setar cor
    $("#branca").click(function(){        
        $("#branca").addClass("selected");
        $("#colorida").removeClass("selected");
        $("#result_cor").html($("#branca").text());
        parametros_pesquisa.cor = "branca";
        atualizarFoto();
        calcularValor();
        atualizarLocalStorage();
    });
    $("#colorida").click(function(){        
        $("#branca").removeClass("selected");
        $("#colorida").addClass("selected");
        $("#result_cor").html($("#colorida").text());
        parametros_pesquisa.cor = "colorida";
        atualizarFoto();
        calcularValor();
        atualizarLocalStorage();
    });

    //Setar gola
    $("#gola_v").click(function(){        
        $("#gola_v").addClass("selected");
        $("#gola_normal").removeClass("selected");
        $("#result_gola").html($("#gola_v").text());
        parametros_pesquisa.gola = "gola_v";
        atualizarFoto();
        calcularValor();
        atualizarLocalStorage();
    });
    $("#gola_normal").click(function(){        
        $("#gola_normal").addClass("selected");
        $("#gola_v").removeClass("selected");
        $("#result_gola").html($("#gola_normal").text());
        parametros_pesquisa.gola = "gola_normal";
        atualizarFoto();
        calcularValor();
        atualizarLocalStorage();
    });

    //Setar qualidade
    $("#q150").click(function(){        
        $("#q150").addClass("selected");
        $("#q190").removeClass("selected");
        $("#result_qualidade").html($("#q150").text());  
        parametros_pesquisa.qualidade = "q150";     
        calcularValor();     
        atualizarLocalStorage();
    });
    $("#q190").click(function(){
        $("#q190").addClass("selected");
        $("#q150").removeClass("selected");
        $("#result_qualidade").html($("#q190").text());
        parametros_pesquisa.qualidade = "q190";
        calcularValor();       
        atualizarLocalStorage(); 
    });

    //Pegar valor select - estampa
    $("#estampa").change(function(){        
        $("#result_estampa").html($('#estampa').find(":selected").text());
        parametros_pesquisa.estampa = $("#estampa").find(":selected").val();
        atualizarFoto();
        calcularValor();
        atualizarLocalStorage();
    });

    //Pegar valor select - embalagem
    $("#embalagem").change(function(){        
        $("#result_embalagem").html($('#embalagem').find(":selected").text());
        parametros_pesquisa.embalagem = $("#embalagem").find(":selected").val();
        calcularValor();
        atualizarLocalStorage();
    });

    //Setar quantidade
    $("#quantidade").on({
        click: function(){            
            $("#result_quantidade").html($(this).val());
            parametros_pesquisa.quantidade = $(this).val();
            calcularValor();
            atualizarLocalStorage();
        },
        keyup: function(){            
            $("#result_quantidade").html($(this).val());
            parametros_pesquisa.quantidade = $(this).val();
            calcularValor();
            atualizarLocalStorage();
        },         
    });

    $("#comprar").click(function(){
        $(".refresh-loader").show();

        window.setTimeout(function(){        
            $(".refresh-loader").hide();
        }, 2000);        
    });*/